
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home/Home.vue'
import Catalog from '../views/Catalog/Catalog.vue' 
import Partners from '../views/Partners/Partners.vue' 
import Blog from '../views/Blog/Blog.vue' 
import Contact from '../views/Contact/Contact.vue' 
import About from '../views/About/About.vue'
import BlogInfo from '../views/BlogInfo/BlogInfo.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/Catalog',
    name: 'Catalog',
    component: Catalog,
  },
  {
    path: '/About',
    name: 'About',
    component: About,
  },
  {
    path: '/catalog-praduct/:postId',
    name: 'CatalogPraduct',
    component: () => import('@/views/CatalogPraduct/CatalogPraduct.vue')
  },
  {
    path: '/Partners',
    name: 'Partners',
    component: Partners
  },
  {
    path: '/Blog',
    name: 'Blog',
    component: Blog
  },
  {
    path: '/BlogInfo',
    name: 'BlogInfo',
    component: BlogInfo
  },
  {
    path: '/Contact',
    name: 'Contact',
    component: Contact
  },
  // {
    // path: '/about',
    // name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
